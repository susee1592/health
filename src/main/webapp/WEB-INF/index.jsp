<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
	"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Health Insurance</title>
</head>
<body>
	<div align="center">
		<form:form action="health" method="post" commandName="customer"
			modelAttribute="customer">
			<table border="0">
				<tr>
					<td colspan="2" align="center"><h2>Get Your Health Insurance Quote</h2></td>
				</tr>
				<tr>
					<td>Name:</td>
					<td><form:input path="name" /></td>				
				</tr>
				<tr>
					<td>Gender:</td>
					<td><form:select path="gender" items="${gender}" /></td>
				</tr>
				<tr>
					<td>Age:</td>
					<td><form:input type="number" path="age" /></td>					
				</tr>
				<tr>
					<td colspan="1" align="left"><h3>Current health</h3></td>
				</tr>
				<tr>
					<td>Hypertension:</td>
					<td><form:radiobutton path="ht" value="Y" />Yes <form:radiobutton
							path="ht" value="N" checked="checked" />No</td>
				</tr>
				<tr>
					<td>Blood pressure:</td>
					<td><form:radiobutton path="bp" value="Y" />Yes <form:radiobutton
							path="bp" value="N" checked="checked" />No</td>
				</tr>
				<tr>
					<td>Blood sugar:</td>
					<td><form:radiobutton path="bs" value="Y" />Yes <form:radiobutton
							path="bs" value="N" checked="checked" />No</td>
				</tr>
				<tr>
					<td>Overweight:</td>
					<td><form:radiobutton path="ow" value="Y" />Yes <form:radiobutton
							path="ow" value="N" checked="checked" />No</td>
				</tr>
				<tr>
					<td colspan="1" align="left"><h3>Habits</h3></td>
				</tr>
						<tr>
					<td>Daily exercise:</td>
					<td><form:radiobutton path="exercise" value="Y" />Yes <form:radiobutton
							path="exercise" value="N" checked="checked" />No</td>
				</tr>
				<tr>
					<td>Smoking:</td>
					<td><form:radiobutton path="smoking" value="Y" />Yes <form:radiobutton
							path="smoking" value="N" checked="checked" />No</td>
				</tr>
				<tr>
					<td>Alcohol:</td>
					<td><form:radiobutton path="alcohol" value="Y" />Yes <form:radiobutton
							path="alcohol" value="N" checked="checked"/>No</td>
				</tr>
		
				<tr>
					<td>Drugs:</td>
					<td><form:radiobutton path="drug" value="Y" />Yes <form:radiobutton
							path="drug" value="N" checked="checked"/>No</td>
				</tr>
				<tr>
				<td colspan="1" align="left"><h3></h3></td>
				</tr>
				<tr>
					<td colspan="2" align="center"><input type="submit"
						value="Submit" /></td>
				</tr>
			</table>
		</form:form>
	</div>
</body>
</html>