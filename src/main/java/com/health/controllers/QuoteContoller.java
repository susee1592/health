package com.health.controllers;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.health.entities.Customer;

@Controller
	public class QuoteContoller {
	
		@RequestMapping(value = "/",method = RequestMethod.GET)
		public String viewCustomer(Model model) {
			
			model.addAttribute("customer", new Customer());
			
			List<String> gender = new ArrayList<String>();
			gender.add("male");
			gender.add("female");
			model.addAttribute("gender", gender);
			
			return "index";
		}
		
		@RequestMapping(value = "/health",method = RequestMethod.POST)
		public String do_calculation(@ModelAttribute("customer") Customer customer, Model model) {
			        if(customer.getAge()==null||customer.getName()==null) {
			            return "error";
			        }else {
			float basePremium = 5000;
			final float fixedbasePremium = 5000;// fixed
			String mr="Ms.";
			if (customer.getAge() >= 18 && customer.getAge() <= 40) {
				basePremium += (0.1 * fixedbasePremium);// 10% +
			} else if (customer.getAge() >40){
				basePremium += (0.2 * fixedbasePremium);// 20% +
			}
			if (customer.getGender().equals("male")) {
				basePremium += (0.02 * fixedbasePremium);// 2% +
				 mr="Mr.";
			}
			if (customer.getHt().equals("Y")) {
				basePremium += (0.01 * fixedbasePremium);// 1% +
			}
			if (customer.getBp().equals("Y")) {
				basePremium += (0.01 * fixedbasePremium);// 1% +
			}
			if (customer.getBs().equals("Y")) {
				basePremium += (0.01 * fixedbasePremium);// 1% +
			}
			if (customer.getOw().equals("Y")) {
				basePremium += (0.01 * fixedbasePremium);// 1% +
			}
			if (customer.getExercise().equals("Y")) {
				basePremium -= (0.03 * fixedbasePremium); // 3% -
			}
			if (customer.getSmoking().equals("Y")) {
				basePremium += (0.03 * fixedbasePremium);// 3% +
			}
			if (customer.getAlcohol().equals("Y")) {
				basePremium += (0.03 * fixedbasePremium);// 3% +
			}
			if (customer.getDrug().equals("Y")) {
				basePremium += (0.03 * fixedbasePremium);// 3% +
			}

			NumberFormat nf = NumberFormat.getInstance();
			model.addAttribute("message", "Health Insurance Premium for " + mr + customer.getName() +": Rs."+  nf.format(basePremium));
		
			return "result";
		}
		}
}
