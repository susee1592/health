package com.health.entities;

public class Customer {
	String name;
	String gender;
	Integer age;
	String ht,bp,bs,ow;
	String smoking,alcohol,exercise,drug;
	
	
	public Customer() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public String getHt() {
		return ht;
	}


	public void setHt(String ht) {
		this.ht = ht;
	}


	public String getBp() {
		return bp;
	}


	public void setBp(String bp) {
		this.bp = bp;
	}


	public String getBs() {
		return bs;
	}


	public void setBs(String bs) {
		this.bs = bs;
	}


	public String getOw() {
		return ow;
	}


	public void setOw(String ow) {
		this.ow = ow;
	}


	public String getSmoking() {
		return smoking;
	}


	public void setSmoking(String smoking) {
		this.smoking = smoking;
	}


	public String getAlcohol() {
		return alcohol;
	}


	public void setAlcohol(String alcohol) {
		this.alcohol = alcohol;
	}


	public String getExercise() {
		return exercise;
	}


	public void setExercise(String exercise) {
		this.exercise = exercise;
	}


	public String getDrug() {
		return drug;
	}


	public void setDrug(String drug) {
		this.drug = drug;
	}


	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	
}
